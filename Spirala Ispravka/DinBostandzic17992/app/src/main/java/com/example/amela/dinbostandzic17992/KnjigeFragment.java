package com.example.amela.dinbostandzic17992;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Amela on 09.04.2018..
 */

public class KnjigeFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        View v=inflater.inflate(R.layout.activity_lista_knjiga_akt, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button povratak = (Button)getActivity().findViewById(R.id.dPovratak);
        ListView listaknjiga = (ListView)getActivity().findViewById(R.id.listaKnjiga);

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*ListeFragment lf=(ListeFragment)getFragmentManager().findFragmentByTag("zube");
                if(lf==null)*/
                   ListeFragment lf=new ListeFragment();
                getFragmentManager().beginTransaction().replace(R.id.Frejm,lf/*,"zube"*/).addToBackStack(null).commit();
            }
        });

        ArrayList<String> string=getArguments().getStringArrayList("key");
        ArrayList<Knjigice> listica = new ArrayList<Knjigice>();
        if((string.get(1)).equals("k")) {
            for (int j = 0; j < KategorijeAkt.knjige.size(); j++) {
                if ((string.get(0)).equals(KategorijeAkt.knjige.get(j).getKategorija()))
                    listica.add(KategorijeAkt.knjige.get(j));
            }
        }
        else
        {
            for (int j = 0; j < KategorijeAkt.knjige.size(); j++) {
                if ((string.get(0)).equals(KategorijeAkt.knjige.get(j).getNativautora()))
                    listica.add(KategorijeAkt.knjige.get(j));
            }
        }
        listaknjiga.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ((Knjigice)adapterView.getItemAtPosition(i)).plavo=true;
                view.setBackgroundColor(R.color.pozadina);
            }
        });
        Kastom adapt = new Kastom(getActivity(),R.layout.sindlerova,listica);
        listaknjiga.setAdapter(adapt);
    }
}

