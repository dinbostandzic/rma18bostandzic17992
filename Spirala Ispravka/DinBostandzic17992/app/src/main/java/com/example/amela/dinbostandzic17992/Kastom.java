package com.example.amela.dinbostandzic17992;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class Kastom extends ArrayAdapter<Knjigice>{

    private Context mContext;
    int mResource;

    public Kastom(Context context,int resource,ArrayList<Knjigice> objects)
    {
        super(context,resource,objects);
        mContext=context;
        mResource=resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        String ime=getItem(position).getNazivknjige();
        String autor=getItem(position).getNativautora();
        String cat = getItem(position).getKategorija();

        Knjigice knjiga = new Knjigice(ime,autor,cat);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);

        TextView enaziv =(TextView) convertView.findViewById(R.id.eNaziv);
        TextView eautor =(TextView) convertView.findViewById(R.id.eAutor);

        enaziv.setText(ime);
        eautor.setText(autor);
        if(getItem(position).plavo)
        {
            convertView.setBackgroundColor(R.color.pozadina);
        }

        return convertView;
    }
}
