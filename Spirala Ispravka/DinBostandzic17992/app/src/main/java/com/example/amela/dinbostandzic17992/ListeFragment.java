package com.example.amela.dinbostandzic17992;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Amela on 09.04.2018..
 */

public class ListeFragment extends Fragment {
    boolean sadrzi=false;
    boolean autorcici=false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        autorcici=false;
        View v=inflater.inflate(R.layout.activity_kategorije_akt, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final Button pretraga=(Button)getActivity().findViewById(R.id.dPretraga);
        final Button dodajkategoriju=(Button)getActivity().findViewById(R.id.dDodajKategoriju);
        final Button dodajknjigu=(Button)getActivity().findViewById(R.id.dDodajKnjigu);
        final EditText tekstpretraga=(EditText)getActivity().findViewById(R.id.tekstPretraga);
        final ListView listakategorija=(ListView)getActivity().findViewById(R.id.listaKategorija);
        final Button kategorije=(Button)getActivity().findViewById(R.id.dKategorije);
        final Button autori=(Button)getActivity().findViewById(R.id.dAutori);
        final Button online=(Button)getActivity().findViewById(R.id.dDodajOnline);
        final ArrayAdapter<String> adapter3=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,KategorijeAkt.kategorije);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, KategorijeAkt.kategorije);
        dodajkategoriju.setEnabled(false);
        dodajknjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autorcici=false;
                DodavanjeKnjigeFragment dkf=(DodavanjeKnjigeFragment) getFragmentManager().findFragmentByTag("dodavanje");
                if(dkf==null)
                    dkf=new DodavanjeKnjigeFragment();
                getFragmentManager().beginTransaction().replace(R.id.Frejm,dkf,"dodavanje").addToBackStack(null).commit();
            }
        });
        pretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sadrzi=false;
                String kategorije2 = tekstpretraga.getText().toString();
                adapter.getFilter().filter(kategorije2);
                listakategorija.setAdapter(adapter);
                dodajkategoriju.setEnabled(false);
                for(int i=0;i<KategorijeAkt.kategorije.size();i++)
                {
                    if((KategorijeAkt.kategorije.get(i)).equals(kategorije2))
                    {
                        sadrzi=true;
                    }
                }
                if(!sadrzi)
                {
                    dodajkategoriju.setEnabled(true);
                }

                adapter.notifyDataSetChanged();
            }
        });
        dodajkategoriju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KategorijeAkt.kategorije.add(tekstpretraga.getText().toString());
                adapter.add(tekstpretraga.getText().toString());
                dodajkategoriju.setEnabled(false);
            }
        });
        listakategorija.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(autorcici) {
                    Bundle args=new Bundle();
                    String imeprezime = KategorijeAkt.autori.get(i).getAutorcic();
                    String a="a";
                    ArrayList<String> string=new ArrayList<String>();
                    string.add(imeprezime);
                    string.add(a);
                    args.putStringArrayList("key",string);
                    /*KnjigeFragment kf=(KnjigeFragment) getFragmentManager().findFragmentByTag("knjige");
                    if(kf==null)*/
                        KnjigeFragment kf=new KnjigeFragment();
                    kf.setArguments(args);
                    getFragmentManager().beginTransaction().replace(R.id.Frejm,kf).addToBackStack(null).commit();

                }
                else
                {
                    Bundle args1=new Bundle();
                    String k="k";
                    String category = (String)adapterView.getItemAtPosition(i);
                    ArrayList<String> tring1=new ArrayList<String>();
                    tring1.add(category);
                    tring1.add(k);
                    args1.putStringArrayList("key",tring1);
                    /*KnjigeFragment kf=(KnjigeFragment) getFragmentManager().findFragmentByTag("knjige");
                    if(kf==null)*/
                        KnjigeFragment kf=new KnjigeFragment();
                    kf.setArguments(args1);
                    getFragmentManager().beginTransaction().replace(R.id.Frejm,kf,"knjige").addToBackStack(null).commit();
                }
            }
        });
        kategorije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                listakategorija.setAdapter(adapter3);
                tekstpretraga.setVisibility(View.VISIBLE);
                pretraga.setVisibility(View.VISIBLE);
                dodajkategoriju.setVisibility(View.VISIBLE);
                autorcici=false;
            }
        });
        final Kastom2 adapter2=new Kastom2(getActivity(),R.layout.sindlerova2,KategorijeAkt.autori);
        autori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autorcici=true;
                listakategorija.setAdapter(adapter2);
                tekstpretraga.setVisibility(View.GONE);
                pretraga.setVisibility(View.GONE);
                dodajkategoriju.setVisibility(View.GONE);
            }
        });
        online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentOnline lf=(FragmentOnline) getFragmentManager().findFragmentByTag("kokolo");
                if(lf==null)
                    lf=new FragmentOnline();
                getFragmentManager().beginTransaction().replace(R.id.Frejm,lf,"kokolo").addToBackStack(null).commit();
            }
        });
    }
}


