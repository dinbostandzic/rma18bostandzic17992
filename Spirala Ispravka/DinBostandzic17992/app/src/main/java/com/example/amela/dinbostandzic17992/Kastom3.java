package com.example.amela.dinbostandzic17992;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Amela on 25.05.2018..
 */

public class Kastom3 extends ArrayAdapter<Knjiga> {
    private Context mContext;
    int mResource;

    public Kastom3(Context context, int resource, ArrayList<Knjiga> objects)
    {
        super(context,resource,objects);
        mContext=context;
        mResource=resource;
    }
    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        String ime=getItem(position).getNaziv();
        String broj=String.valueOf(getItem(position).getBroj());
        ArrayList<Autor> lista=getItem(position).getAutori();
        String opis = getItem(position).getOpis();
        String datum =getItem(position).getDatumObjavljivanja();
        URL slika=getItem(position).getSlika();


        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);

        TextView NAZ =(TextView)convertView.findViewById(R.id.textView4);
        TextView IME =(TextView)convertView.findViewById(R.id.textView6);
        TextView DATUM =(TextView)convertView.findViewById(R.id.eDatumObjavljivanja);
        TextView OP=(TextView)convertView.findViewById(R.id.eOpis);
        ImageView SL=(ImageView)convertView.findViewById(R.id.imageView2);
        TextView BRS=(TextView)convertView.findViewById(R.id.eBrojStranica);
        Button preporuci=(Button)convertView.findViewById(R.id.dPreporuci);
        TextView D=(TextView)convertView.findViewById(R.id.editText6);
        TextView B=(TextView)convertView.findViewById(R.id.editText7);
        TextView O=(TextView)convertView.findViewById(R.id.editText8);
        D.setText("Datum objavljivanja:");
        B.setText("Broj stranica:");
        O.setText("Opis:");
        NAZ.setText(ime);
        DATUM.setText(datum);
        OP.setText(opis);
        BRS.setText(broj);
        String autor="";
        for(int i=0;i<lista.size();i++)
        {
            autor=autor+lista.get(i).getImeiPrezime();
        }
        IME.setText(autor);

        if(getItem(position).plava)
        {
            convertView.setBackgroundColor(R.color.pozadina);
        }
        if(slika!=null) {
            Picasso.get().load(slika.toString()).into(SL);
        }
        preporuci.setFocusable(false);
        preporuci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args1=new Bundle();
                args1.putParcelable("knjiga",getItem(position));
                FragmentPreporuci fg=new FragmentPreporuci();
                fg.setArguments(args1);
                ((KategorijeAkt)mContext).getFragmentManager().beginTransaction().replace(R.id.Frejm,fg).addToBackStack(null).commit();
            }
        });
        return convertView;
    }
}
