package com.example.amela.dinbostandzic17992;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

/**
 * Created by Amela on 18.05.2018..
 */

public class FragmentOnline extends Fragment implements DohvatiKnjige.IDohvatiKnjigeDone,DohvatiNajnovije.IDohvatiNajnovijeDone{
    boolean novi=true;
    ArrayAdapter<String> adapter;
    ArrayList<ArrayList<Knjiga>> knjizica=new ArrayList<ArrayList<Knjiga>>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v=inflater.inflate(R.layout.fragment_online, container, false);
        final Spinner kategorija = (Spinner)v.findViewById(R.id.sKategorije);
        final ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, KategorijeAkt.kategorije);
        kategorija.setAdapter(adapter2);
        return v;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button run =(Button)getActivity().findViewById(R.id.dRun);
        Button dodaj =(Button)getActivity().findViewById(R.id.dAdd);
        Button povratak =(Button)getActivity().findViewById(R.id.dPovratak);
        final EditText knjiga = (EditText)getActivity().findViewById(R.id.tekstUpit);
        final Spinner kategorija = (Spinner)getActivity().findViewById(R.id.sKategorije);

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.Frejm,new ListeFragment()).addToBackStack(null).commit();
            }
        });
        run.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                knjizica =new ArrayList<ArrayList<Knjiga>>();
                if(knjiga.getText().toString().length()>6 && knjiga.getText().toString().substring(0,6).equals("autor:"))
                {
                    new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone)FragmentOnline.this).execute(knjiga.getText().toString().substring(6));
                }
                else if(knjiga.getText().toString().contains(";"))
                {
                    ArrayList<String> niz = new ArrayList<String>();
                    int i=0;
                    char a=';';
                    while(i<knjiga.getText().toString().length())
                    {
                        int j=i;
                        while(i!=knjiga.getText().toString().length() && knjiga.getText().toString().charAt(i)!=a)
                        {
                            i++;
                        }
                        niz.add(knjiga.getText().toString().substring(j,i));
                        i++;
                    }
                    for(int k=0;k<niz.size();k++)
                    {
                        new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone) FragmentOnline.this).execute(niz.get(k));
                    }
                }
                else {
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone) FragmentOnline.this).execute(knjiga.getText().toString());
                }
            }
        });
        dodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Spinner rezultat =(Spinner)getActivity().findViewById(R.id.sRezultat);
                String knjiga = rezultat.getSelectedItem().toString();
                ArrayList<Knjiga> knjizica1=new ArrayList<Knjiga>();
                for(int i=0;i<knjizica.size();i++)
                {
                    for(int j=0;j<knjizica.get(i).size();j++) {
                        knjizica1.add(knjizica.get(i).get(j));
                    }
                }
                for(int i=0;i<knjizica1.size();i++)
                {
                    if(knjiga.equals(knjizica1.get(i).getNaziv())){
                        for(int j=0;j<knjizica1.get(i).getAutori().size();j++)
                        {
                            KategorijeAkt.knjige.add(new Knjigice(knjiga,knjizica1.get(i).getAutori().get(j).getImeiPrezime(),kategorija.getSelectedItem().toString()));
                        }
                        for(int j=0;j<knjizica1.get(i).getAutori().size();j++) {
                            for (int k = 0; k < KategorijeAkt.autori.size(); k++) {
                                if (knjizica1.get(i).getAutori().get(j).getImeiPrezime().equals(KategorijeAkt.autori.get(k).getAutorcic())) {
                                    int p = KategorijeAkt.autori.get(k).getBrojknjiga();
                                    KategorijeAkt.autori.get(k).setBrojknjiga(p + 1);
                                    novi = false;
                                }
                            }
                            if (novi) {
                                KategorijeAkt.autori.add(new Autori(knjizica1.get(i).getAutori().get(j).getImeiPrezime(), 1));
                            }
                        }
                    }
                }
            }
        });
    }
        @Override
        public void onDohvatiDone(ArrayList<Knjiga> knjizurina){
            ArrayList<String> s=new ArrayList<String>();
            knjizica.add(knjizurina);
            Spinner rezultat = (Spinner)getActivity().findViewById(R.id.sRezultat);
            for(int i=0;i<knjizica.size();i++)
            {
                for(int j=0;j<knjizica.get(i).size();j++) {
                    s.add(knjizica.get(i).get(j).getNaziv());
                }
            }
            adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, s);
            rezultat.setAdapter(adapter);
        }
    @Override
    public void onNajnovijeDone(ArrayList<Knjiga> knjizurina){
        ArrayList<String> s=new ArrayList<String>();
        knjizica.add(knjizurina);
        Spinner rezultat = (Spinner)getActivity().findViewById(R.id.sRezultat);
        for(int i=0;i<knjizica.size();i++)
        {
            for(int j=0;j<knjizica.get(i).size();j++) {
                s.add(knjizica.get(i).get(j).getNaziv());
            }
        }
        adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, s);
        rezultat.setAdapter(adapter);
    }
}
