package com.example.amela.dinbostandzic17992;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

/**
 * Created by Amela on 09.04.2018..
 */

public class DodavanjeKnjigeFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v=inflater.inflate(R.layout.activity_dodavanje_knjige_akt, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button ponisti =(Button)getActivity().findViewById(R.id.dPonisti);
        Button nadjisliku =(Button)getActivity().findViewById(R.id.dNadjiSliku);
        Button upisiknjigu =(Button)getActivity().findViewById(R.id.dUpisiKnjigu);
        final Spinner kat = (Spinner)getActivity().findViewById(R.id.sKategorijaKnjige);
        final EditText imeautora = (EditText)getActivity().findViewById(R.id.imeAutora);
        final EditText nazivknjige = (EditText)getActivity().findViewById(R.id.nazivKnjige);
        ImageView naslovnastr = (ImageView)getActivity().findViewById(R.id.naslovnaStr);
        final ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, KategorijeAkt.kategorije);
        kat.setAdapter(adapter2);

        ponisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.Frejm,new ListeFragment()).addToBackStack(null).commit();
            }
        });

        upisiknjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean novi=true;
                String ime=imeautora.getText().toString();
                String naziv=nazivknjige.getText().toString();
                String prazan="";
                if(!(ime.equals(prazan)) && !(naziv.equals(prazan))) {
                    KategorijeAkt.knjige.add(new Knjigice(naziv, ime, kat.getSelectedItem().toString()));
                    for(int i=0;i<KategorijeAkt.autori.size();i++)
                    {
                        if(ime.equals(KategorijeAkt.autori.get(i).getAutorcic())){
                            int p=KategorijeAkt.autori.get(i).getBrojknjiga();
                            KategorijeAkt.autori.get(i).setBrojknjiga(p+1);
                            novi=false;
                        }
                    }
                    if(novi)
                    {
                        KategorijeAkt.autori.add(new Autori(ime,1));
                    }
                }

            }
        });
    }
}

