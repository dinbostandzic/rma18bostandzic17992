package com.example.amela.dinbostandzic17992;

/**
 * Created by Amela on 21.03.2018..
 */

public class Knjigice {
    private String nazivknjige;
    private String nazivautora;
    private String kategorija;
    boolean plavo;
    Knjigice(String naziv,String autor,String katkat)
    {
        nazivknjige=naziv;
        nazivautora=autor;
        kategorija=katkat;
        plavo=false;
    }
    public String getNazivknjige() {
        return nazivknjige;
    }

    public void setNazivknjige(String nazivknjige) {
        this.nazivknjige = nazivknjige;
    }

    public String getNativautora() {
        return nazivautora;
    }

    public void setNativautora(String nativautora) {
        this.nazivautora = nativautora;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }
}
