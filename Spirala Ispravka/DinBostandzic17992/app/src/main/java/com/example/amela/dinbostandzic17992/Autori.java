package com.example.amela.dinbostandzic17992;

/**
 * Created by Amela on 12.04.2018..
 */

public class Autori {
    private String autorcic;
    private int brojknjiga;

    public Autori(String autor,int broj)
    {
        autorcic=autor;
        brojknjiga=broj;
    }
    public String getAutorcic() {
        return autorcic;
    }

    public void setAutorcic(String autorcic) {
        this.autorcic = autorcic;
    }

    public int getBrojknjiga() {
        return brojknjiga;
    }

    public void setBrojknjiga(int brojknjiga) {
        this.brojknjiga = brojknjiga;
    }
}
