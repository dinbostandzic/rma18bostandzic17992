package com.example.amela.dinbostandzic17992;

/**
 * Created by Amela on 30.05.2018..
 */

public class Autorstvo {
    private String knjigaid;
    private String autorid;

    public Autorstvo(String knjigaid, String autorid) {
        this.knjigaid = knjigaid;
        this.autorid = autorid;
    }

    public String getKnjigaid() {
        return knjigaid;
    }

    public void setKnjigaid(String knjigaid) {
        this.knjigaid = knjigaid;
    }

    public String getAutorid() {
        return autorid;
    }

    public void setAutorid(String autorid) {
        this.autorid = autorid;
    }
}
