package com.example.amela.dinbostandzic17992;

import java.util.ArrayList;

/**
 * Created by Amela on 18.05.2018..
 */

public class Autor {
    private String imeiPrezime;
    private ArrayList<String> knjige=new ArrayList<String>();

    public Autor(String imeiPrezime,String knjige){
        this.imeiPrezime=imeiPrezime;
        this.knjige.add(0,knjige);
    }

    void dodajKnjigu(String id) {
        for (int i = 0; i < knjige.size(); i++) {
            if (knjige.get(i).equals(id)) {
                return;
            }
        }
        knjige.add(id);
    }

    public String getImeiPrezime() {
        return imeiPrezime;
    }

    public void setImeiPrezime(String imeiPrezime) {
        this.imeiPrezime = imeiPrezime;
    }

    public ArrayList<String> getKnjige() {
        return knjige;
    }

    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }
}
