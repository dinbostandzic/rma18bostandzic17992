package com.example.amela.dinbostandzic17992;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ListaKnjigaAkt extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_knjiga_akt);

        Button povratak = (Button) findViewById(R.id.dPovratak);
        ListView listaknjiga = (ListView) findViewById(R.id.listaKnjiga);

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent povratak2=new Intent(ListaKnjigaAkt.this,KategorijeAkt.class);
                startActivity(povratak2);
            }
        });
        Intent i = getIntent();
        String s1=i.getStringExtra("category");
        ArrayList<Knjigice> listica = new ArrayList<Knjigice>();
        for(int j=0;j<KategorijeAkt.knjige.size();j++)
        {
            if(s1.equals(KategorijeAkt.knjige.get(j).getKategorija()))
                listica.add(KategorijeAkt.knjige.get(j));
        }
        listaknjiga.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ((Knjigice)adapterView.getItemAtPosition(i)).plavo=true;
                view.setBackgroundColor(0xffaabbed);
            }
        });
        Kastom adapt = new Kastom(this,R.layout.sindlerova,listica);
        listaknjiga.setAdapter(adapt);
    }
}
