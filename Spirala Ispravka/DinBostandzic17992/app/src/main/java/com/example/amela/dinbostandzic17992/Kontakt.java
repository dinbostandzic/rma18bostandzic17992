package com.example.amela.dinbostandzic17992;

import java.util.ArrayList;

/**
 * Created by Amela on 28.05.2018..
 */

public class Kontakt {
    public String kontakt;
    public ArrayList<String> email;

    public Kontakt(String kontakt, ArrayList<String> email) {
        this.kontakt = kontakt;
        this.email = email;
    }

    public String getKontakt() {
        return kontakt;
    }

    public void setKontakt(String kontakt) {
        this.kontakt = kontakt;
    }

    public ArrayList<String> getEmail() {
        return email;
    }

    public void setEmail(ArrayList<String> email) {
        this.email = email;
    }
}
