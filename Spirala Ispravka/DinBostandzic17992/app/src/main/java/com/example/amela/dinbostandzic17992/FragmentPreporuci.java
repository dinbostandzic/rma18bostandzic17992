package com.example.amela.dinbostandzic17992;

import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Amela on 25.05.2018..
 */

public class FragmentPreporuci extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_preporuci, container, false);
        String email;
        ArrayList<Kontakt> emailarray = new ArrayList<Kontakt>();
        KategorijeAkt.kontakti=new ArrayList<Kontakt>();
        emailarray=ShowContact();
        ArrayList<String> imena=new ArrayList<String>();
        for(int i=0;i<emailarray.size();i++)
        {
            imena.add(emailarray.get(i).getKontakt());
        }
        ArrayList<String> emailovi = new ArrayList<String>();
        for(int i=0;i<emailarray.size();i++)
        {
            for(int j=0;j<emailarray.get(i).getEmail().size();j++)
            {
                emailovi.add(emailarray.get(i).getEmail().get(j));
            }
        }
        Spinner kontakti=(Spinner)v.findViewById(R.id.sKontakti);
        final ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, emailovi);
        kontakti.setAdapter(adapter3);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button send=(Button)getActivity().findViewById(R.id.dPosalji);
        TextView NAZIV =(TextView)getActivity().findViewById(R.id.textView);
        TextView IMEAUTORA =(TextView)getActivity().findViewById(R.id.textView2);
        TextView DATUMOBJAVE =(TextView)getActivity().findViewById(R.id.textView3);
        TextView OPIS=(TextView)getActivity().findViewById(R.id.textView5);
        ImageView SLIKA=(ImageView)getActivity().findViewById(R.id.imageView);
        TextView BRSTR=(TextView)getActivity().findViewById(R.id.brstr);
        TextView N=(TextView)getActivity().findViewById(R.id.editText2);
        TextView I=(TextView)getActivity().findViewById(R.id.editText3);
        TextView D=(TextView)getActivity().findViewById(R.id.editText4);
        TextView B=(TextView)getActivity().findViewById(R.id.editText10);
        TextView O=(TextView)getActivity().findViewById(R.id.editText5);
        N.setText("Naziv:");
        I.setText("Ime autora:");
        D.setText("Datum objavljivanja:");
        B.setText("Broj stranica:");
        O.setText("Opis:");

        final Knjiga knjiza =getArguments().getParcelable("knjiga");

        NAZIV.setText(knjiza.getNaziv());
        String autor="";
        ArrayList<Autor> lista=knjiza.getAutori();
        for(int i=0;i<lista.size();i++)
        {
            autor=autor+lista.get(i).getImeiPrezime();
        }
        IMEAUTORA.setText(autor);
        DATUMOBJAVE.setText(knjiza.getDatumObjavljivanja());
        OPIS.setText(knjiza.getOpis());
        BRSTR.setText(String.valueOf(knjiza.getBroj()));
        if(knjiza.getSlika()!=null) {
            Picasso.get().load(knjiza.getSlika().toString()).into(SLIKA);
        }


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Spinner kontakti1=(Spinner)getActivity().findViewById(R.id.sKontakti);
                int i=kontakti1.getSelectedItemPosition();
                String[] TO = {kontakti1.getSelectedItem().toString()};

                String mejl="Zdravo "+ KategorijeAkt.kontakti.get(i).getKontakt()+",\nProcitaj knjigu " + knjiza.getNaziv()+" od "+knjiza.getAutori().get(0).getImeiPrezime()+"!";
                Intent emailintent=new Intent(Intent.ACTION_SEND);
                emailintent.setData(Uri.parse("mailto"));
                emailintent.setType("text/plain");
                emailintent.putExtra(Intent.EXTRA_EMAIL,TO);
                emailintent.putExtra(Intent.EXTRA_CC,"");
                emailintent.putExtra(Intent.EXTRA_SUBJECT,"");
                emailintent.putExtra(Intent.EXTRA_TEXT,mejl);
                try{
                    startActivity(Intent.createChooser(emailintent,"Send email..."));
                    getActivity().finish();
                }
                catch(android.content.ActivityNotFoundException ex){
                    Toast.makeText(getActivity(),"There is no email client installed",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public ArrayList<Kontakt> ShowContact() {

        ArrayList<String> nameList = new ArrayList<String>();
        ArrayList<String> emailList = new ArrayList<String>();

        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur
                        .getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur
                        .getString(cur
                                .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer
                        .parseInt(cur.getString(cur
                                .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    // Query phone here. Covered next

                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                    + " = ?", new String[] { id }, null);
                    while (pCur.moveToNext()) {
                        // Do something with phones
                        String phoneNo = pCur
                                .getString(pCur
                                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                        nameList.add(name); // Here you can list of contact.


                        Cursor emailCur = cr.query(
                                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                                new String[]{id}, null);
                        ArrayList<String> provjera= new ArrayList<String>();
                        while (emailCur.moveToNext()) {
                            String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

                            emailList.add(email); // Here you will get list of email
                            provjera.add(email);
                        }
                        if(provjera.size()>0)
                        {
                            KategorijeAkt.kontakti.add(new Kontakt(name,provjera));
                        }
                        emailCur.close();
                    }
                    pCur.close();
                }
            }
        }

        return KategorijeAkt.kontakti; // here you can return whatever you want.
    }
}
