package com.example.amela.dinbostandzic17992;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Amela on 12.04.2018..
 */

public class Kastom2 extends ArrayAdapter<Autori> {
    private Context mContext;
    int mResource;

    public Kastom2(Context context, int resource, ArrayList<Autori> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String ime = getItem(position).getAutorcic();
        int brojknjiga = getItem(position).getBrojknjiga();

        Autori autor = new Autori(ime, brojknjiga);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView Autor = (TextView) convertView.findViewById(R.id.Autor);
        TextView Broj = (TextView) convertView.findViewById(R.id.Broj);
        String broj = brojknjiga + "";
        Autor.setText(ime);
        Broj.setText(broj);
        return convertView;
    }
}
