package com.example.amela.dinbostandzic17992;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Amela on 18.05.2018..
 */

public class DohvatiKnjige extends AsyncTask<String,Integer,Void> {

    @Override
    protected Void doInBackground(String... params){
        String query=null;
        try{
            query= URLEncoder.encode(params[0],"utf-8");
        }
        catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
        String url1="https://www.googleapis.com/books/v1/volumes?q=intitle:"+query+"&maxResults=5";
        try{
            URL url=new URL(url1);
            HttpURLConnection urlConnection=(HttpURLConnection)url.openConnection();
            InputStream in=new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo= new JSONObject(rezultat);
            JSONArray items;
            String id;
            JSONObject volumeinfo;
            String naziv;
            JSONArray autori;
            String description;
            String datum;
            int brstr;
            JSONObject slika;
            String link;
            if(jo.has("items")) {
                items = jo.getJSONArray("items");
            }
            else
            {
                items=null;
            }
            if(items!=null) {
                for (int i = 0; i < items.length(); i++) {
                    JSONObject objekat = items.getJSONObject(i);
                    if (objekat.has("id")) {
                        id = objekat.getString("id");
                    } else {
                        id = null;
                    }
                    if (objekat.has("volumeInfo")) {
                        volumeinfo = objekat.getJSONObject("volumeInfo");
                    } else {
                        volumeinfo = null;
                    }
                    if (volumeinfo.has("title")) {
                        naziv = volumeinfo.getString("title");
                    } else {
                        naziv = null;
                    }
                    if (volumeinfo.has("authors")) {
                        autori = volumeinfo.getJSONArray("authors");
                    } else {
                        autori = null;
                    }
                    if (volumeinfo.has("description")) {
                        description = volumeinfo.getString("description");
                    } else {
                        description = null;
                    }
                    if (volumeinfo.has("publishDate")) {
                        datum = volumeinfo.getString("publishedDate");
                    } else {
                        datum = null;
                    }
                    if (volumeinfo.has("pageCount")) {
                        brstr = volumeinfo.getInt("pageCount");
                    } else {
                        brstr = 0;
                    }
                    if (volumeinfo.has("imageLinks")) {
                        slika = volumeinfo.getJSONObject("imageLinks");
                    } else {
                        slika = null;
                    }
                    if (slika != null && slika.has("thumbnail")) {
                        link = slika.getString("thumbnail");
                    } else {
                        link = null;
                    }

                    URL slajba = null;
                    if (link != null) slajba = new URL(link);

                    ArrayList<Autor> a = new ArrayList<Autor>();
                    if (autori != null)
                        for (int j = 0; j < autori.length(); j++) {
                            a.add(new Autor(autori.get(j).toString(), id));
                        }
                    knjigica.add(new Knjiga(id, naziv, a, description, datum, slajba, brstr));
                }
            }
        }
        catch(MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    public String convertStreamToString(InputStream is){
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try{
            while((line=reader.readLine())!=null){
                sb.append(line+"\n");
            }
        }
        catch(IOException e){e.printStackTrace();}
        finally{
            try {
                is.close();
            }
            catch(IOException e){
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    public interface IDohvatiKnjigeDone{
        public void onDohvatiDone(ArrayList<Knjiga> knjigica);
    }
    ArrayList<Knjiga> knjigica=new ArrayList<Knjiga>();
    private IDohvatiKnjigeDone pozivatelj;
    public DohvatiKnjige(IDohvatiKnjigeDone p){
        pozivatelj=p;
    }
    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        pozivatelj.onDohvatiDone(knjigica);
;    }
}
