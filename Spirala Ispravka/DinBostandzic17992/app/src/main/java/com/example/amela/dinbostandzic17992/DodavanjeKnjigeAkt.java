package com.example.amela.dinbostandzic17992;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import java.lang.String;
public class DodavanjeKnjigeAkt extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodavanje_knjige_akt);


        Button ponisti =(Button) findViewById(R.id.dPonisti);
        Button nadjisliku =(Button) findViewById(R.id.dNadjiSliku);
        Button upisiknjigu =(Button) findViewById(R.id.dUpisiKnjigu);
        final Spinner kat = (Spinner) findViewById(R.id.sKategorijaKnjige);
        final EditText imeautora = (EditText) findViewById(R.id.imeAutora);
        final EditText nazivknjige = (EditText) findViewById(R.id.nazivKnjige);
        ImageView naslovnastr = (ImageView) findViewById(R.id.naslovnaStr);
        final ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, KategorijeAkt.kategorije);
        kat.setAdapter(adapter2);

        ponisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ponistavanje=new Intent(DodavanjeKnjigeAkt.this,KategorijeAkt.class);
                startActivity(ponistavanje);
            }
        });

        upisiknjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ime=imeautora.getText().toString();
                String naziv=nazivknjige.getText().toString();
                String prazan="";
                if(!(ime.equals(prazan)) && !(naziv.equals(prazan))) {
                    KategorijeAkt.knjige.add(new Knjigice(ime, naziv, kat.getSelectedItem().toString()));
                }
            }
        });

    }
}
